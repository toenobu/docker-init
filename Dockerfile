#
FROM alpine:3.13

RUN apk add --no-cache ca-certificates

ADD echo5.sh /echo5.sh
RUN chmod u+x /echo5.sh

CMD ["/echo5.sh"]
