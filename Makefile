.PHONY: build-image run-image run-image-with-init login-image login-image-with-init

APP_NAME = echo5

build-image:
	docker build --rm -t $(APP_NAME) .

run-image:
	docker run --name ec --rm $(APP_NAME)

run-image-with-init:
	docker run --init --name ec-init --rm $(APP_NAME)

attach-process:
	docker exec -it ec /bin/sh

attach-process-with-init:
	docker exec -it ec-init /bin/sh

##### compose

run-compose:
	docker compose up --remove-orphans
