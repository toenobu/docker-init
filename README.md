# docker-init
docker init

https://christina04.hatenablog.com/entry/docker-init

# build
`make build-image`

# run image with init optiono
`make run-image-with-init`

# login the image
`make attach-process-with-init`

# run image with compose
`make run-compose`